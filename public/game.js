//game.js
var socket = io();

var movement = {
    up: false,
    down: false,
    left: false,
    right: false
}

var fire = false;

document.addEventListener('keydown', function (event) {
    switch (event.keyCode) {
        case 65: // A
            movement.left = true;
            break;
        case 87: // W
            movement.up = true;
            break;
        case 68: // D
            movement.right = true;
            break;
        case 83: // S
            movement.down = true;
            break;
        case 88: // X
            //movement.space = true;
            fire = true;
            socket.emit('fire', fire);
            break;

    }
});
document.addEventListener('keyup', function (event) {
    switch (event.keyCode) {
        case 65: // A
            movement.left = false;
            break;
        case 87: // W
            movement.up = false;
            break;
        case 68: // D
            movement.right = false;
            break;
        case 83: // S
            movement.down = false;
            break;
        case 88: // X
            // movement.space = true;
            fire = false;
            socket.emit('fire', fire);
            break;
    }
});


// var input = document.getElementById("input"),
//     output = document.getElementById("output");

// input.addEventListener("keyup", function () {
//     output.innerText = input.value;
//     var data = input.value;
//     // 1. Отправка о подключении игрока
//     socket.emit('new player', data);
//     // -----
// }, false);

socket.emit('new player');



socket.on('message', function (data) {
    console.log(data);
});

// 2. Отправка нажатых клавиш
setInterval(function () {
    socket.emit('movement', movement);
}, 1000 / 10);
// -----

var canvas = document.getElementById('canvas');

canvas.width = 400;
canvas.height = 400;
var context = canvas.getContext('2d');

// TODO: Добавление карт в БД

// Для отображения матрицы на поле
context.fillText("Alphabetic", 400, 100);
context.textBaseline = "hanging";
context.textAlign = "start";
context.font = "12px Verdana";
// ------

// 3. Прнятие ткущего положения игроков, карты
socket.on('state', function (players, playing_field) {

    context.clearRect(0, 0, 400, 400);

    // Отрисовка карты
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {

            switch (playing_field[i][j]) {
                case 1:
                    //console.log("true");
                    context.fillStyle = '#fbc02d';

                    context.fillRect(j * 40, i * 40, 38, 38);

                    context.fillStyle = 'white';
                    context.fillText(playing_field[i][j], j * 40, i * 40);
                    break;
                case 0:
                    //console.log("false");
                    context.fillStyle = '#4caf50';
                    context.fillRect(j * 40, i * 40, 38, 38);

                    context.fillStyle = 'black';
                    context.fillText(playing_field[i][j], j * 40, i * 40);
                    break;
                case 6:
                    //console.log("false");
                    context.fillStyle = 'purple';
                    context.fillRect(j * 40, i * 40, 38, 38);

                    context.fillStyle = 'black';
                    context.fillText(playing_field[i][j], j * 40, i * 40);
                    break;
                case 5:
                    //console.log("false");
                    context.fillStyle = '#e65100';
                    context.fillRect(j * 40, i * 40, 38, 38);

                    context.fillStyle = 'white';
                    context.fillText(playing_field[i][j], j * 40, i * 40);
                    break;
                case 8:
                    //console.log("false");
                    context.fillStyle = '#304ffe';
                    context.fillRect(j * 40, i * 40, 38, 38);

                    context.fillStyle = 'white';
                    context.fillText(playing_field[i][j], j * 40, i * 40);
                    break;
                case 7:
                    //console.log("false");
                    context.fillStyle = 'purple';
                    context.fillRect(j * 40, i * 40, 38, 38);

                    context.fillStyle = 'white';
                    context.fillText(playing_field[i][j], j * 40, i * 40);
                    break;

                default:
                    break;
            }
        }
    }
    // ------

    // Вывод статистики
    var tableArr = ['<table>'];
    for (var id in players) {
        tableArr.push(
            '<tr>' +
            '<td>' +
            'I =' + players[id].i +
            '</td>' +
            '<td>' +
            'J =' + players[id].j +
            '</td>' +
            '<td>' +
            ' Health = ' + players[id].health +
            '</td>' +
            '</tr>'
        );
    }
    tableArr.push('</table>');
    document.getElementById('info').innerHTML = tableArr.join('\n')
    // ------

});
// -----

// 4. Отправка сигнала отключения пользователя
window.onunload = function () {
    socket.emit('disconnect');
};