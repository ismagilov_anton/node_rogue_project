module.exports = class Player {
    constructor
        (
            j, // Положение игрока по J
            i, // Положение игрока по I
            side,
            fire,
            health, ) {
        this.j = j;
        this.i = i;
        this.side = side;
        this.fire = fire;
        this.health = health;
    }
}