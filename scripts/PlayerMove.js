

exports.left = function (player, playing_field) {
    //console.log(player);
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            var temp = playing_field[player.i][player.j - 1];
            playing_field[player.i][player.j] = playing_field[player.i][player.j - 1];
            playing_field[player.i][player.j] = temp;
        }
    }
    player.j--;
    player.side = "left_side";
}

exports.right = function (player, playing_field) {
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            var temp = playing_field[player.i][player.j + 1];
            playing_field[player.i][player.j] = playing_field[player.i][player.j + 1];
            playing_field[player.i][player.j] = temp;
        }
    }
    player.j++;
    player.side = "right_side";
}

exports.up = function (player, playing_field) {
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            var temp = playing_field[player.i - 1][player.j];
            playing_field[player.i][player.j] = playing_field[player.i - 1][player.j + 1];
            playing_field[player.i][player.j] = temp;
        }
    }
    player.i--;
    player.side = "up_side";
}

exports.down = function (player, playing_field) {
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            var temp = playing_field[player.i + 1][player.j];
            playing_field[player.i][player.j] = playing_field[player.i + 1][player.j + 1];
            playing_field[player.i][player.j] = temp;
        }
    }
    player.i++;
    player.side = "down_side";
}