// Проверка на наличие бонуса
exports.left = function (player, playing_field) {
    //console.log(data);

    if (playing_field[player.i][player.j - 1] == 8) {
        playing_field[player.i][player.j] = 0;
        player.health += 25;
        player.j--;
        player.side = "left_side";
        return true;
    }
}
exports.right = function (player, playing_field) {
    if (playing_field[player.i][player.j + 1] == 8) {
        playing_field[player.i][player.j] = 0;
        player.health += 25;
        player.j++;
        player.side = "right_side";
        return true;
    }
}
exports.up = function (player, playing_field) {
    if (playing_field[player.i - 1][player.j] == 8) {
        playing_field[player.i][player.j] = 0;
        player.health += 25;
        player.i--;
        player.side = "up_side";
        return true;
    }
}
exports.down = function (player, playing_field) {
    if (playing_field[player.i + 1][player.j] == 8) {
        playing_field[player.i][player.j] = 0;
        player.health += 25;
        player.i++;
        player.side = "down_side";
        return true;
    }
}
// -----