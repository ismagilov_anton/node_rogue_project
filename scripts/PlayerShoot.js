

exports.left = function (player, playing_field) {
    console.log("Player shoot left");
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            playing_field[player.i][player.j - 1] = 7;
            setTimeout(function () {
                playing_field[player.i][player.j - 1] = 0;
            }, 1000 / 10);
        }
    }
}

exports.right = function (player, playing_field) {
    console.log("Player shoot right");
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            playing_field[player.i][player.j + 1] = 7;
            setTimeout(function () {
                playing_field[player.i][player.j + 1] = 0;
            }, 1000 / 10);
        }
    }
}


exports.up = function (player, playing_field) {
    console.log("Player shoot up");
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            playing_field[player.i - 1][player.j] = 7;
            setTimeout(function () {
                playing_field[player.i - 1][player.j] = 0;
            }, 1000 / 10);
        }
    }
}

exports.down = function (player, playing_field) {
    console.log("Player shoot down");
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            playing_field[player.i + 1][player.j] = 7;
            setTimeout(function () {
                playing_field[player.i + 1][player.j] = 0;
            }, 1000 / 10);
        }
    }
}