// Проверка на наличие стен
exports.left = function (player, playing_field) {
    if (player.j > 0) {
        if (playing_field[player.i][player.j - 1] == 0) {
            // Проверка на стенку
            return true;
        }
    }
}


exports.right = function (player, playing_field) {
    if (player.j < 9) {
        if (playing_field[player.i][player.j + 1] == 0) {
            // Проверка на стенку
            return true;
        }
    }
}


exports.up = function (player, playing_field) {
    if (player.j > 0) {
        if (playing_field[player.i - 1][player.j] == 0) {
            // Проверка на стенку
            return true;
        }
    }
}


exports.down = function (player, playing_field) {
    if (player.j < 9) {
        if (playing_field[player.i + 1][player.j] == 0) {
            // Проверка на стенку
            return true;
        }
    }
}
// -----