//server.js
// Dependencies
// TODO: Добавить паттерн и рефакторинг
// https://socket.io/docs/server-api/#namespace-use-fn
var express = require('express');
var http = require('http');
var path = require('path');
var socketIO = require('socket.io'); var app = express();
var server = http.Server(app);
var io = socketIO(server); app.set('port', 5000);

app.set("view engine", "hbs");

const hbs = require("hbs");
hbs.registerPartials(__dirname + "/views");


app.use('/public', express.static(__dirname + '/public'));// Routing

app.get('/', function (request, response) {
    response.render("index.hbs");
});// Starts the server.

server.listen(5000, function () {
    console.log('Now you can connect: http://127.0.0.1:5000/');
});













// https://medium.com/freecodecamp-russia-%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D1%8F%D0%B7%D1%8B%D1%87%D0%BD%D1%8B%D0%B9/%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B3%D0%BE-%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F-%D0%B4%D0%BB%D1%8F-%D1%87%D0%B0%D1%82%D0%B0-%D1%81-%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C%D1%8E-node-js-%D0%B8-socket-io-eb7498391611

var playing_field = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 8, 0, 1, 1, 0, 1],
    [1, 0, 1, 1, 0, 0, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 8, 0, 0, 8, 0, 0, 8, 0, 1],
    [1, 0, 1, 1, 0, 0, 1, 1, 0, 1],
    [1, 0, 1, 1, 8, 0, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
];

var players = {};

var movePlayer = require("./scripts/PlayerMove.js");
var checkWall = require("./scripts/WallCheck.js");
var checkBonus = require("./scripts/BonusCheck.js");
var shootPlayer = require("./scripts/PlayerShoot.js");


const Player = require("./models/Player.js");

io.on('connection', function (socket) {
    //console.log("New Player connected");

    // 1. Принятие подключенного игрока
    // TODO: Как различать пользователей
    socket.on('new player', function () {
        var new_player = new Player(
            3, // Положение игрока по Jasdw
            4, // Положение игрока по I
            "",
            null,
            100,
        );
        players[socket.id] = new_player;
        console.log(players);
    });
    // -----

    // 4. Принятие сигнала отключения пользователя
    socket.on('disconnect', function () {
        var player = players[socket.id]
        playing_field[player.i][player.j] = 0;
        delete players[socket.id];
        console.log(players);
    });
    // FIXME: Если игроки на одной клетке то не удаляются
    // -----

    // 2. Принятие нажатых клавиш
    socket.on('movement', function (data) {
        var player = players[socket.id] || {};
        // TODO: Перемещает даже других игроков
        playing_field[player.i][player.j] = 5;
        movement(player, playing_field, data);
    });
    // -----

    // 5. Принятие открытия огня
    socket.on('fire', function (data) {
        var player = players[socket.id] || {};
        // Удаление 
        shooting(player, playing_field);

    });
    // -----
});

// 3. Отправка текущего положения игроков, карты
setInterval(function () {
    io.sockets.emit('state', players, playing_field);
}, 1000 / 60);
// ------

// TODO: Rooms
// https://overcoder.net/q/264630/nodejs-%D0%B8-socketio-%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BA%D0%BE%D0%BC%D0%BD%D0%B0%D1%82%D1%8B

function movement(player, playing_field, data) {
    if (data.left) { // A
        if (checkWall.left(player, playing_field) == true) {
            movePlayer.left(player, playing_field)
            checkBonus.left(player, playing_field);
        }
    }
    if (data.right) { // D
        if (checkWall.right(player, playing_field) == true) {
            movePlayer.right(player, playing_field)
            checkBonus.right(player, playing_field);
        }
    }
    if (data.up) { // W
        if (checkWall.up(player, playing_field) == true) {
            movePlayer.up(player, playing_field)
            checkBonus.up(player, playing_field);
        }
    }
    if (data.down) { // S
        if (checkWall.down(player, playing_field) == true) {
            movePlayer.down(player, playing_field)
            checkBonus.down(player, playing_field);
        }
    }
}

function shooting(player, playing_field) {
    switch (player.side) {
        case "left_side":
            if (checkWall.left(player, playing_field) == true) {
                shootPlayer.left(player, playing_field);
            }
            break;
        case "right_side":
            if (checkWall.right(player, playing_field) == true) {
                shootPlayer.right(player, playing_field);
            }
            break;
        case "up_side":
            if (checkWall.up(player, playing_field) == true) {
                shootPlayer.up(player, playing_field);
            }
            break;
        case "down_side":
            if (checkWall.down(player, playing_field) == true) {
                shootPlayer.down(player, playing_field);
            }
            break;
        default:
            break;
    }
}